# XTestLeptonRequest

Private module study for [LEPTON-CMS][1] 7.1  
_This is nothing more than a private playground._

### license
- cc 3.0 by-sa;

### Requirements
- [LEPTON CMS][1], Version >= 7.1
- PHP 8.2 (8.3 recommended)

### E.g. use inside code2
```php
$oTest = XTestLeptonRequest::getInstance();

$testValues = [
    // -- [1]
    'integer' => [
        'value'   => 1234,
        'type'    => 'integer',
        'default' => 0
    ],
    // -- [2]
    'integer_plus' => [
        'value'   => -2,
        'type'    => 'integer+',
        'default' => 12
    ],
    // -- [3]
    'date' => [
        'value'   => '1824-02-30',
        'type'    => 'date',
        'default' => '1966-03-11'
    ],
    // -- [4]
    'datetime' => [
        'value'   => date('Y-m-d H:i:s'),
        'type'    => 'datetime',
        'default' => '1812-01-07 00:00:00'
    ],
    // -- [5]
    'integer_eval' => [
        'value'   => '12 * (2 + 2)',
        'type'    => 'integer_eval',
        'default' => 0
    ],
    // -- [6]   However, 'time' is stored with seconds in db! (12:40:00)
    'time' => [
        'value'   => '12:40',
        'type'    => 'time',
        'default' => 0
    ]
];
$oTest->executeTests($testValues);

```

![console output](/img/screen_output.png)  
  
2024.8 - Aldus

[1]: https://lepton-cms.org "LEPTON CMS"
