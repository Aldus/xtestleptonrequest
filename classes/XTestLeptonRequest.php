<?php

declare(strict_types=1);

/**
 *  @module         xtestLeptonRequest
 *  @authors        Dietrich Roland Pehlke (Aldus)
 *  @license        cc 3.0 by-sa;
 *  @license terms  https://creativecommons.org/licenses/by/3.0/
 *
 */

class XTestLeptonRequest extends LEPTON_abstract
{

    public const TESTTABLE = "xtest_lepton_request";
    
    public array $tableInfo = [];
    public array $fieldDefaults = [];
    
    public static $instance;
    
    #[\Override]
    public function initialize(): void
    {
        LEPTON_database::getInstance()->describe_table(
            TABLE_PREFIX.self::TESTTABLE,
            $this->tableInfo,
            LEPTON_database::DESCRIBE_ASSOC
        );
        
        $this->getFieldDefaults();
    }
    
    public function getFieldNames(): array
    {
        $allFields = array_keys($this->tableInfo);
        array_shift($allFields); // extract the first element [id]
        array_shift($allFields); // skip [generated_when]
        return $allFields;
    }
    
    /**
    
     $testValues = [
        'integer'       => ['value' => 1234,            'type' => 'integer', 'default' => 0],
        'integer_plus'  => ['value' => -2,              'type' => 'integer+', 'default' => 12],
        'date'          => ['value' => '2024-02-30',    'type' => 'date', 'default' => '1907-01-01'],
        'datetime'      => ['value' => date('Y-m-d H:i:s'), 'type' => 'datetime', 'default' => '1812-01-07 00:00:00'],
        'integer_eval'  => ['value' => '12 * (2 + 2)',  'type' => 'integer_eval', 'default' => 0]
    ];
    
    **/
    
    public function executeTests(array $testValues): void
    {
        // 1 prepare the post
        $postedValues = [];
        foreach ($testValues as $item => $data)
        {
            $_POST[$item] = $data['value'];
            $postedValues[$item] = $data['value'];
        }

        $this->messageHead("\$_POST");
        echo LEPTON_tools::display($postedValues, "pre", "ui message orange");
    
        // 2 get request from post via L*request
        $oREQUEST = LEPTON_request::getInstance();
        
        $allTestedValues = $oREQUEST->testPostValues($testValues);
        
        $this->messageHead("After \$_POST");
        echo LEPTON_tools::display($allTestedValues, "pre", "ui message olive");
        
        // 3 try to insert into db
        
        $database = LEPTON_database::getInstance();
        $database->build_and_execute(
            'insert',
            TABLE_PREFIX.self::TESTTABLE,
            $allTestedValues
        );
        
        // 4 read from db
        $allInEntry = [];
        $fields = "`".implode("`, `", array_keys($testValues))."`";
        $fields = "`id`, `generated_when`, ".$fields;
        $database->execute_query(
            "SELECT ".$fields." from `".TABLE_PREFIX.self::TESTTABLE."` ORDER BY `id` DESC limit 1",
            true,
            $allInEntry,
            false
        );
        
        // 5 display result
        $this->messageHead("DB entries:");
        echo LEPTON_tools::display($allInEntry, "pre", "ui message green");
    }
    
    /**
    
        $dataProvider = [
            'Name vom Test' => [
                'integer'   => ['value' => 1234, 'type' => 'integer', 'default' => 0]
            ],
            'Anderer Name vom Test' => [
                'integer'   => ['value' => 0123, 'type' => 'integer', 'default' => NULL]
            ],
            'Noch ein Test' => [
                'date'   => ['value' => "2023-03-11", 'type' => 'date', 'default' => "1970-01-01"]
            ],
        ];
        
    **/
    public function messageHead(string $message=""): void
    {
        echo LEPTON_tools::display($message, "pre", "ui message white");
    }
    
    /**
     * for the use of PhpUnit 11.x
     *
     */
    public static function fillSuperGlobal(string $where, string $name, string|int|array $value): void
    {
        switch (strtolower($where))
        {
            case 'post':
                $_POST[$name] = $value;
                break;

            case 'get':
                $_GET[$name] = $value;
                break;

            case 'session':
                $_SESSION[strtoupper($name)] = $value;
                break;

            case 'server':
                $_SERVER[$name] = $value;
                break;
        }
    }
    
    public static function tempSuperGlobalUnit(string $lookForName)
    {
        $result = filter_input(INPUT_POST, $lookForName, FILTER_VALIDATE_INT);
        return $result;   
    }
    
    protected function getFieldDefaults(): void
    {
        foreach ($this->tableInfo as $fieldName => $values)
        {
            $this->fieldDefaults[ $fieldName ] = $values['Default'];
        }
    }
}