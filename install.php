<?php

/**
 *  @module         xtestLeptonRequest
 *  @authors        Dietrich Roland Pehlke (Aldus)
 *  @license        cc 3.0 by-sa;
 *  @license terms  https://creativecommons.org/licenses/by/3.0/
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// create new table
$table_fields="
    `id`            int(11)     unsigned NOT NULL AUTO_INCREMENT,
    `generated_when` timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `integer`       int(11)     unsigned NOT NULL DEFAULT '0',
    `integer_plus`  int(11)     NOT NULL DEFAULT '1',
    `integer_minus` int(11)     NOT NULL DEFAULT '-1',
    `number`        int(11)     NOT NULL DEFAULT '0',
    `integer_eval`  int(11)     NOT NULL DEFAULT '0',
    `decimal`       decimal(10,2)   NOT NULL DEFAULT '0.00',
    `float`         float(10,4)     NOT NULL DEFAULT '0.0000',
    `double`        double      NOT NULL DEFAULT '0',
    `string`            varchar(128) NOT NULL DEFAULT '',
    `string_clean`      varchar(128) NOT NULL DEFAULT '',
    `string_chars`      varchar(128) NOT NULL DEFAULT '',
    `string_allowed`    varchar(128) NOT NULL DEFAULT '',
    `string_secure`     varchar(128) NOT NULL DEFAULT '',
    `email`     varchar(255)    NOT NULL DEFAULT '',
    `date`      date            DEFAULT '1970-01-01',
    `datetime`  datetime        NOT NULL DEFAULT '1970-01-01 00:00:00',
    `time`      time            NOT NULL DEFAULT '00:00:00',
    `bool`      tinyint(1)      NOT NULL DEFAULT '1',
    `regexp`    varchar(25)     NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)"
;

LEPTON_handle::install_table(
    XTestLeptonRequest::TESTTABLE,
    $table_fields
);
