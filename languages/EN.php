<?php

/**
 *  @module         xtestLeptonRequest
 *  @authors        Dietrich Roland Pehlke (Aldus)
 *  @license        cc 3.0 by-sa;
 *  @license terms  https://creativecommons.org/licenses/by/3.0/
 *
 */

$MOD_XTESTLEPTONREQUEST = [
    'hello' => "Hello world. Here i'm: just a private study for L* VII."
];
