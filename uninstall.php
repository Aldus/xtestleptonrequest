<?php

/**
 *  @module         xtestLeptonRequest
 *  @authors        Dietrich Roland Pehlke (Aldus)
 *  @license        cc 3.0 by-sa;
 *  @license terms  https://creativecommons.org/licenses/by/3.0/
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$database->execute_query("DROP TABLE `".TABLE_PREFIX.XTestLeptonRequest::TESTTABLE."`");

